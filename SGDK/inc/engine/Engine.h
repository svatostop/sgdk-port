#ifndef ENGINE_H
#define ENGINE_H

#include "../genesis.h"
#include "../../res/resources.h"
#include "Level.h"
#include "Player.h"
#include "Enemy.h"

#define CHOOSE_PLAYER -1
#define GAME_HOW_TO 0
#define GAME_INTRO 1
#define GAME_START 2

extern int gameStatus;
extern bool jump;

bool keyHandle(Level *level);
void gameModeHandle(Level *level);

void introInit(Level *level);
void choosePlayer(Level *level);

bool collisionLefiSide(Level *level, u16 value);
bool collisionRightSide(Level *level, u16 value);

void initEnemy(Level *level);
void initSprite(Level *level);
bool enemyProcessing(Level *level);
bool checkEnemyCollision(Level *level, u16 offset);
void setBallPos(Level *level);


Sprite *getCurrentSprite(Level *level);
void jumpAction(Level *level);

void setSpriteAnimation(Level *level, s16 anim, bool active, u16 curFrame);
bool playAnim(Level *level);

void checkDefinitionBg(Level *level);

void playerAttackProcessing(Level *level);

void initBg(Level *level);
void clearBg(Level *level);
void drawLevel(Level *level);

#endif