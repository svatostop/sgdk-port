#ifndef PLAYER_H
#define PLAYER_H

#include "genesis.h"

#define ANIM_IDLE 0
#define ANIM_MOVE 1

#define ANIM_JUMP 2
#define ANIM_ATTACK 3
#define ANIM_ATTACK_UP 4

typedef struct
{
	Sprite *spriteDef;
	Sprite *spriteHealth;

	bool playAnim;
	bool attack;
	int animInd;
	int counterAnim;
	int counterAnimCheck;

	int x;
	int y;

	u16 health;
	
	int height;
	int width;

	int plPosX;

	int plPosY;

	bool playerW;

	int jumpNew;
	bool jumpCollision;

} Player;

#endif