#ifndef LEVEL_H
#define LEVEL_H

#include "genesis.h"

#include "Player.h"
#include "Enemy.h"


typedef struct
{
	Map *backGroundBg;
	Map *backGroundMap;

	Player player;
	Enemy enemy;

	int level;

	int newCamX;
	int newCamY;
	u16 rand;

	u16 frame;
	Palette curPalBg;
	Palette curPalMap;
	Palette palPlayer;
	Palette palPlayerW;

	TileSet curTileBg;
	MapDefinition curMapBg;

	TileSet curTileMap;
	MapDefinition curMapMap;

} Level;

#endif