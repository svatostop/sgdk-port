#ifndef ENEMY_H
#define ENEMY_H

#include "genesis.h"

typedef struct
{
	Sprite *fireBallSpr;

	int xPos;
	int yPos;

	bool active;
	bool kill;

	int xPosTmp;
	int yPosTmp;
	u16 w;
	u16 h;
	u16 tile[2];
	u16 health;

} Tree;

typedef struct
{
	Tree tree;
} Enemy;

#endif