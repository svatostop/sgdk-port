TILESET introFont "image/bmp/introFont.png" BEST NONE

PALETTE palIntroPl "image/bmp/introBGPlayers.png"

Sprite introPlayer "image/bmp/introBGPlayers.png" 17 18 FAST 5

SPRITE playerSpr "image/bmp/animationPlayerMen.png" 6 6 BEST 6
SPRITE playerSprW "image/bmp/animationPlayerWomen.png" 6 6 BEST 6

SPRITE menHealth "image/bmp/menHealth.png" 4 4 BEST 0
SPRITE womenHealth "image/bmp/womenHealth.png" 4 4 BEST 0


SPRITE fireBall "image/bmp/fireBall.png" 4 4 BEST 5

IMAGE img "image/bmp/introBG.png" BEST ALL
IMAGE imgChoose "image/bmp/choosePlayer.png" BEST ALL


SPRITE arrow "image/bmp/arrow.png" 3 3 FAST 5

PALETTE enemyPal "image/bmp/fireBall.png" 
PALETTE arrowPal "image/bmp/arrow.png"
PALETTE palAll "image/bmp/bg1.png"
PALETTE palAllMap "image/bmp/1.png"
PALETTE palPlayer "image/bmp/animationPlayerMen.png"
PALETTE palPlayerW "image/bmp/animationPlayerWomen.png"


TILESET tilesetFirstLvlBg "image/bmp/bg1.png" BEST ALL
MAP bgFirstLvl "image/bmp/bg1.png" tilesetFirstLvlBg BEST 

TILESET tilesetFirstLvlMap "image/bmp/1.png" BEST ALL
MAP mapFirstLvl "image/bmp/1.png" tilesetFirstLvlMap BEST 

