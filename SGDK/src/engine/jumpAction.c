#include "engine/Engine.h"
bool jump;
void jumpAction(Level *level)
{
 	if (!jump)
   {
   	u16 collisionInd = 0;

		collisionInd = MAP_getTile(level->backGroundMap, ((level->player.plPosX)/8), ((level->player.plPosY+level->player.height)/8));

		if ((collisionInd > 68  || collisionInd < 65))
		{
			collisionInd = MAP_getTile(level->backGroundMap, ((level->player.plPosX+level->player.width)/8), ((level->player.plPosY+level->player.height)/8));
		}

 		if ((collisionInd <= 68  && collisionInd >= 65) || (collisionInd <= 167  && collisionInd >= 158))
   	{
      	jump = FALSE;
      	level->player.jumpNew = level->player.y - 16;
      	level->player.jumpCollision = FALSE;
      	level->player.animInd = -1;
      	return ;
   	}
   	else
   	{
   		// if (level->player.animInd != ANIM_JUMP)
     //  		level->player.animInd = -1;

   		if (level->player.animInd != ANIM_JUMP)
   		{
   			SPR_setFrame(level->player.spriteDef,  9);
   			//level->player.animInd = ANIM_JUMP;
   		}
   	
   		level->player.jumpCollision = TRUE;
   		level->player.plPosY += 1;
   		level->player.y += 1;
      }
	 
   }
   if (jump)
   {
   	if (level->player.plPosY <= (level->player.jumpNew))
   	{
   		jump = FALSE;

   	}
   	level->player.jumpCollision = TRUE;
   	level->player.plPosY -= 1;
   	level->player.y -= 1;

   }
}
