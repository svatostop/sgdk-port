#include "engine/Engine.h"
bool jump;

void initBg(Level *level)
{
   u16 ind = TILE_USERINDEX;

   u16 bgBaseTileIndex[2];

   bgBaseTileIndex[0] = ind;
   VDP_loadTileSet(&(level->curTileMap), ind, DMA);
   ind += (level->curTileMap).numTile;
   bgBaseTileIndex[1] = ind;
   VDP_loadTileSet(&(level->curTileBg), ind, DMA);
   ind += (level->curTileBg).numTile;

   VDP_setPalette(PAL0, level->curPalBg.data);
   VDP_setPalette(PAL1, level->curPalMap.data);

   level->backGroundMap = MAP_create(&(level->curMapMap), BG_A, TILE_ATTR_FULL(PAL1, FALSE, FALSE, FALSE, bgBaseTileIndex[0]));
   level->backGroundBg = MAP_create(&(level->curMapBg), BG_B, TILE_ATTR_FULL(PAL0, FALSE, FALSE, FALSE, bgBaseTileIndex[1]));
   DMA_setBufferSize(9000);

   level->newCamX = 0;
   level->newCamY = 0;
   MAP_scrollTo(level->backGroundMap, fix16ToInt(level->newCamX), level->newCamY);

   MAP_scrollTo(level->backGroundBg, 0, 0);

   SYS_doVBlankProcess();
   DMA_setBufferSizeToDefault();
   
}

void initSprite(Level *level)
{
	 if (level->player.playerW)
   	VDP_setPalette(PAL2, level->palPlayerW.data);
   else
   	VDP_setPalette(PAL2, level->palPlayer.data);


   level->player.jumpNew = 120 - 16;
   level->player.playAnim = FALSE;
   level->frame = 0;

   level->player.x = 60;
   level->player.y = 120;
   level->player.width = 16;
   level->player.height = 30;

   level->player.plPosX = 60 + (level->player.width);
   level->player.plPosY = 120 +  (48 - level->player.height);

   if (level->player.playerW)
   {
		level->player.spriteDef = SPR_addSprite(&playerSprW, level->player.x, level->player.y, TILE_ATTR(PAL2, TRUE, FALSE, FALSE));
      level->player.spriteHealth = SPR_addSprite(&womenHealth, 10, 10, TILE_ATTR(PAL2, TRUE, FALSE, FALSE));
   }
	else
	{
		level->player.spriteDef = SPR_addSprite(&playerSpr, level->player.x, level->player.y, TILE_ATTR(PAL2, TRUE, FALSE, FALSE));
      level->player.spriteHealth = SPR_addSprite(&menHealth, 10, 10, TILE_ATTR(PAL2, TRUE, FALSE, FALSE));
	}
   level->player.health = 0;
   level->player.attack = FALSE;
	level->player.jumpCollision = FALSE;
}

void initEnemy(Level *level)
{
   VDP_setPalette(PAL3, enemyPal.data);

   level->enemy.tree.xPos = 10;
   level->enemy.tree.yPos = 10;
   level->enemy.tree.tile[0] = 56;
   level->enemy.tree.tile[1] = 144;
   level->enemy.tree.health = 1;

   level->enemy.tree.fireBallSpr = SPR_addSprite(&fireBall, level->enemy.tree.xPos, level->enemy.tree.yPos, TILE_ATTR(PAL3, TRUE, FALSE, FALSE));
   SPR_setVisibility(level->enemy.tree.fireBallSpr, HIDDEN);
}

void clearBg(Level *level)
{
	MEM_free(level->backGroundBg);
	MEM_free(level->backGroundMap);
}

void checkDefinitionBg(Level *level)
{
	if (level->level == 0)
	{
		level->curPalBg = palAll;
		level->curPalMap = palAllMap;
		level->palPlayer = palPlayer;
		level->palPlayerW = palPlayerW;

		level->curTileBg = tilesetFirstLvlBg;
		level->curMapBg = bgFirstLvl;

		level->curTileMap = tilesetFirstLvlMap;
		level->curMapMap = mapFirstLvl;
	}
}

void drawLevel(Level *level)
{
	initBg(level);
	initSprite(level);
   initEnemy(level);

   jump = FALSE;
   level->player.animInd = -1;

   while (1)
   {
  
  		jumpAction(level);
  		keyHandle(level);
      enemyProcessing(level);
      
   	SPR_setPosition(getCurrentSprite(level), level->player.x, level->player.y);
      SPR_setPosition(level->enemy.tree.fireBallSpr, level->enemy.tree.xPos-level->newCamX, level->enemy.tree.yPos);
      SPR_update();

      SYS_doVBlankProcess();
   }
   clearBg(level);
}
