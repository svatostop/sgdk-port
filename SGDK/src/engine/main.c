#include "engine/Engine.h"

int gameStatus;

int main()
{   
   gameStatus = CHOOSE_PLAYER;

   Level level;

   level.level = 0;
   level.frame = 0;
   level.rand = 0;
   checkDefinitionBg(&level);

   SPR_init();

   while (1)
   {

      gameModeHandle(&level);
   }

   return (0);
}
