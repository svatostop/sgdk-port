#include "engine/Engine.h"
int gameStatus;
bool jump;


bool keyHandle(Level *level)
{
   u16 value = JOY_readJoypad(JOY_1);
   bool moveR = FALSE;
   bool moveL = FALSE;


   if (level->player.counterAnimCheck != 3)
   {
      moveL = collisionLefiSide(level, value);
      moveR = collisionRightSide(level, value);
   } 

   if (level->player.y > level->player.jumpNew && !jump && (value & BUTTON_A) && !level->player.jumpCollision)
   {
      jump = TRUE;
      level->player.plPosY -= 1;
      level->player.y -= 1;
      level->player.animInd = ANIM_JUMP;

      //setSpriteAnimation(level, ANIM_JUMP, TRUE, 0);
     // SPR_setFrame(level->player.spriteDef, 0);
      SPR_setAnim(level->player.spriteDef, ANIM_JUMP);
      level->player.jumpCollision = TRUE;

      if (level->player.attack)
      {
         //SPR_setAnim(level->player.spriteDef, ANIM_IDLE);

         level->player.attack = FALSE;

         level->player.counterAnimCheck = 0;
      }


      return TRUE;
   }

   level->rand = random() % 2;

   if ((value & BUTTON_B) &&  ( !level->player.jumpCollision)&& (!level->player.attack)  && level->rand == 0)
   {
      //SPR_setAnim(level->player.spriteDef, );
      setSpriteAnimation(level, ANIM_ATTACK, TRUE, 0);

     // setSpriteAnimation(level, ANIM_ATTACK, TRUE, 0);
      level->player.attack = TRUE;
      return TRUE;
   }
   else if ((value & BUTTON_B) &&  ( !level->player.jumpCollision) && (!level->player.attack) && level->rand == 1)
   {
     // SPR_setAnim(level->player.spriteDef, ANIM_ATTACK_UP);

      setSpriteAnimation(level, ANIM_ATTACK_UP, TRUE, 0);
      level->player.attack = TRUE;
      return TRUE;
   }

   if (playAnim(level))
      return TRUE;
   

   if (!moveL && !moveR )
   {
      SPR_setAnim(level->player.spriteDef, ANIM_IDLE);
   }

   return FALSE;

}

void gameModeHandle(Level *level)
{
   if (gameStatus == GAME_INTRO)
   {
     introInit(level);
   }
   if (gameStatus == GAME_START)
   {
      drawLevel(level);
   }
   if (gameStatus == CHOOSE_PLAYER)
      choosePlayer(level);

}
