#include "engine/Engine.h"
bool jump;

Sprite *getCurrentSprite(Level *level)
{
	//if (SPR_getVisibility(level->player.spriteDef) == VISIBLE)
		return level->player.spriteDef;
	//else
		//return level->player.spriteAction;
}

void setSpriteAnimation(Level *level, s16 anim, bool active, u16 curFrame)
{
	// if (!active && (anim == ANIM_IDLE || anim == ANIM_MOVE))
	// {
	// 	SPR_setAnim(level->player.spriteDef, anim);
	// }
	if (active && (anim == ANIM_ATTACK || anim == ANIM_ATTACK_UP))
	{

		SPR_setAnim(level->player.spriteDef, anim);

		if (anim == ANIM_ATTACK || anim == ANIM_ATTACK_UP)
		{
			level->player.counterAnim = 3;
		}
		// if (anim == ANIM_JUMP)
		// 	level->player.counterAnim = 5;
		
		level->player.counterAnimCheck = level->player.counterAnim;
		level->player.playAnim = TRUE;
		level->frame = curFrame;
	}
}

bool playAnim(Level *level)
{
	if (!level->player.playAnim)
		return FALSE;



	if (level->player.attack)
      playerAttackProcessing(level);
	int maxFrame = level->player.spriteDef->animation->numFrame;

	if (level->frame < maxFrame)
    {
         level->player.counterAnim--;
         if (level->player.counterAnim < 0)
            level->player.counterAnim = level->player.counterAnimCheck;

         SPR_setFrame(level->player.spriteDef,  level->frame);
    
         if (level->player.counterAnim == 0)
         {
            level->frame++;
         }


    }
    else
    {
    	level->player.playAnim = FALSE;
		level->player.counterAnimCheck = 0;
		level->player.attack = FALSE;
    }
    return TRUE;
}