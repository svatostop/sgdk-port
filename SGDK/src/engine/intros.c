#include "engine/Engine.h"
int gameStatus;

void choosePlayer(Level *level)
{
   VDP_drawImageEx(BG_B, &imgChoose, TILE_ATTR_FULL(PAL0, TRUE, FALSE, FALSE, TILE_USERINDEX), 0, 0, TRUE, CPU);
   
   VDP_setPalette(PAL1, arrowPal.data);

   u16 posX = 120;
   u16 posY = 170;
   Sprite* arrowSpr = SPR_addSprite(&arrow, posX,posY, TILE_ATTR(PAL1, TRUE, FALSE, FALSE));

   while (1)
   {
      u16 value = JOY_readJoypad(JOY_1);
      if (value & BUTTON_LEFT && (posX > 120))
      {
         posX-=65;
         SPR_setPosition(arrowSpr,  posX, posY);

      }
      if (value & BUTTON_RIGHT && (posX < 185))
      {
         posX+=65;
         SPR_setPosition(arrowSpr,  posX, posY);

      }
      if (value & BUTTON_START)
      {
         if (posX == 120)
         {
            level->player.playerW = TRUE;
         }
         else
            level->player.playerW = FALSE;

         gameStatus = GAME_INTRO;
         VDP_resetScreen();
         SPR_releaseSprite(arrowSpr);

         SYS_doVBlankProcess();
         return ;

      }
         SPR_update();
         SYS_doVBlankProcess();
   }
}

void introInit(Level *level)
{
   char *str[10] = {"THE EVIL SKULL KNIGHT", "DARKOR HAS SNATCHED THE", "BLACK JEWEL, OLD OBSCURE",
   "FORCES AWAKENED UNDER", "HIS ENORMOUS POWER.", "IT IS UP TO YOU RYAN TO", "START YOUR JOURNEY AND",
   "STOP HIS THREAT BRINGING", "BACK PEACE TO THE", "KINGDOM."};

   //VDP_setPlaneSize(64,32,TRUE);

   VDP_loadFont(&introFont, 1);

   VDP_setPaletteColor(32+1,RGB24_TO_VDPCOLOR(0xffffff));
   VDP_setTextPalette(PAL2);
   VDP_setTextPlane(BG_B);

   VDP_drawImageEx(BG_A, &img, TILE_ATTR_FULL(PAL0, TRUE, FALSE, FALSE, TILE_USERINDEX), 0, 0, TRUE, CPU);
   
   VDP_setPalette(PAL1, palIntroPl.data);

   Sprite* player = SPR_addSprite(&introPlayer, 88,24, TILE_ATTR(PAL1, TRUE, FALSE, FALSE));

   SPR_update();

   s16 x = 9;
   s16 y = 28;
   u16 ind = 0;
   fix16 offsetY = FIX16(0);
   fix16 offsetY_save = FIX16(0);
   
   u16 scroll_txt = 0;

   while (ind < 4)
   {
      VDP_drawText(str[ind], x, y);
      ind++;
      y++;
   }
   y = 0;

   while (1)
   {
      
     if(scroll_txt >= 8 && ind < 10){
        scroll_txt = 0;
        VDP_drawText(str[ind], x, y);
        ind++;
        y++;
     }
     
     u16 value = JOY_readJoypad(JOY_1);
   
      if (value & BUTTON_START || fix16ToInt(offsetY) >= 120)
      {

         gameStatus = GAME_START;
         VDP_resetScreen();
         SPR_releaseSprite(player);

         SYS_doVBlankProcess();
         return ;
      }

      offsetY = fix16Add(offsetY,FIX16(0.1));
     if(offsetY_save != offsetY){
        offsetY_save = offsetY;
        scroll_txt++;
     }
        
     VDP_setVerticalScroll(BG_B,  fix16ToInt(offsetY));
      SYS_doVBlankProcess();

   }

}
