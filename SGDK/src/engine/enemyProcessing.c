#include "engine/Engine.h"

bool checkEnemyCollision(Level *level, u16 offset)
{
   u16 collisionInd = 0;

   collisionInd = MAP_getTile(level->backGroundMap, ((level->player.plPosX+level->player.width+offset)/8), ((level->player.plPosY+level->player.height-8)/8));
        // KLog_U1("R1 -------------",collisionInd);
   
   if (collisionInd == level->enemy.tree.tile[0] || collisionInd == level->enemy.tree.tile[1])
      return TRUE;
   collisionInd = MAP_getTile(level->backGroundMap, ((level->player.plPosX+level->player.width+offset)/8), ((level->player.plPosY+level->player.height-16)/8));
        // KLog_U1("R2 -------------",collisionInd);
   
  if (collisionInd == level->enemy.tree.tile[0] || collisionInd == level->enemy.tree.tile[1])
     return TRUE;
   collisionInd = MAP_getTile(level->backGroundMap, ((level->player.plPosX+level->player.width+offset)/8), ((level->player.plPosY+level->player.height-32)/8));
        // KLog_U1("R3 -------------",collisionInd);
   
   if (collisionInd == level->enemy.tree.tile[0] || collisionInd == level->enemy.tree.tile[1])
      return TRUE;


   return FALSE;
}

void setBallPos(Level *level)
{
	u16 randY = random() % 40;

	if (randY <= 15)
	randY += 5;

	level->enemy.tree.xPos = level->enemy.tree.xPosTmp;
	level->enemy.tree.yPos = level->enemy.tree.yPosTmp;

	level->enemy.tree.h = level->enemy.tree.yPos - randY;
	level->enemy.tree.w = level->enemy.tree.xPos - (level->enemy.tree.h*4);

	level->enemy.tree.active = FALSE;
	SPR_setPosition(level->enemy.tree.fireBallSpr, level->enemy.tree.xPos, level->enemy.tree.yPos);
}

void ballAttack(Level *level)
{


   if ((level->enemy.tree.yPos >= level->enemy.tree.h) && !level->enemy.tree.active)
      level->enemy.tree.yPos--;
   else
   {
      u16 collisionInd = 0;
      level->enemy.tree.active = TRUE;

      collisionInd = MAP_getTile(level->backGroundMap, ((level->enemy.tree.xPos)/8), ((level->enemy.tree.yPos+24)/8));
      if ((collisionInd <= 68  && collisionInd >= 65) || (collisionInd <= 167  && collisionInd >= 158))
      {
         if (level->enemy.tree.kill)
         {
            SPR_setVisibility(level->enemy.tree.fireBallSpr, HIDDEN);
            return ;
         }
         setBallPos(level);
      }
      else  if (((level->enemy.tree.xPos+10 <= level->player.plPosX+32 && level->enemy.tree.xPos+10 >= level->player.plPosX)
   	  && (level->enemy.tree.yPos+10 >= level->player.plPosY && level->enemy.tree.yPos+10 <= level->player.plPosY+30))
      || ((level->enemy.tree.xPos+22 <= level->player.plPosX+16 && level->enemy.tree.xPos+22 >= level->player.plPosX)
   	  && (level->enemy.tree.yPos+22 >= level->player.plPosY && level->enemy.tree.yPos+22 <= level->player.plPosY+30)))
      {
         level->player.health++;
         if (level->player.health >= level->player.spriteHealth->animation->numFrame)
            level->player.health = 0;
         SPR_nextFrame(level->player.spriteHealth);
         setBallPos(level);
      }
      else
      {
         level->enemy.tree.yPos++;
      }

   }

   if (level->enemy.tree.xPos >= level->enemy.tree.w)
      level->enemy.tree.xPos--;
}

bool enemyProcessing(Level *level)
{
   

   if (SPR_getVisibility(level->enemy.tree.fireBallSpr) == VISIBLE)
   {

      if (level->player.plPosX - level->enemy.tree.xPosTmp >= 100)
      {
         level->enemy.tree.kill = TRUE;
      }
      ballAttack(level);
      return FALSE;
   }

   if (checkEnemyCollision(level, 96))
      level->enemy.tree.xPos = level->player.plPosX+96;
   else if (checkEnemyCollision(level, 8))
      level->enemy.tree.xPos = level->player.plPosX+8;
   else
      return FALSE;


   SPR_setVisibility(level->enemy.tree.fireBallSpr, VISIBLE);

   level->enemy.tree.kill = FALSE;
   level->enemy.tree.yPos = level->player.y;
   level->enemy.tree.xPosTmp = level->enemy.tree.xPos;
   level->enemy.tree.yPosTmp = level->enemy.tree.yPos;
   level->enemy.tree.w =  level->enemy.tree.xPos - 96 ;
   level->enemy.tree.h = level->enemy.tree.yPos - 48;
   level->enemy.tree.active = FALSE;


   SPR_setPosition(level->enemy.tree.fireBallSpr, level->enemy.tree.xPos, level->enemy.tree.yPos);
   return TRUE;
}
