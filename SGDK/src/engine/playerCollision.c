#include "engine/Engine.h"

void playerAttackProcessing(Level *level)
{
   if (((level->enemy.tree.xPos+10 <= level->player.plPosX+32 && level->enemy.tree.xPos+10 >= level->player.plPosX)
     && (level->enemy.tree.yPos+10 >= level->player.plPosY && level->enemy.tree.yPos+10 <= level->player.plPosY+30))
   || ((level->enemy.tree.xPos+22 <= level->player.plPosX+16 && level->enemy.tree.xPos+22 >= level->player.plPosX)
     && (level->enemy.tree.yPos+22 >= level->player.plPosY && level->enemy.tree.yPos+22 <= level->player.plPosY+30)))
   {
        KLog_U1("R2 -------------",level->enemy.tree.xPos);

      setBallPos(level);
   }

}

bool collisionLefiSide(Level *level, u16 value)
{

   if (!(value & BUTTON_LEFT))
   {
      return FALSE;
   }

   u16 collisionInd = MAP_getTile(level->backGroundMap, ((level->player.plPosX-1)/8), ((level->player.plPosY+level->player.height-8)/8));
   
   if ((collisionInd <= 68  && collisionInd >= 65) || (collisionInd <= 167  && collisionInd >= 158))
      return FALSE;
   collisionInd = MAP_getTile(level->backGroundMap, ((level->player.plPosX-1)/8), ((level->player.plPosY+level->player.height-16)/8));
  
   if ((collisionInd <= 68  && collisionInd >= 65) || (collisionInd <= 167  && collisionInd >= 158))
      return FALSE;
   collisionInd = MAP_getTile(level->backGroundMap, ((level->player.plPosX-1)/8), ((level->player.plPosY+level->player.height-32)/8));
   
   if ((collisionInd <= 68  && collisionInd >= 65) || (collisionInd <= 167  && collisionInd >= 158))
      return FALSE;
   
   collisionInd = MAP_getTile(level->backGroundMap, ((level->player.plPosX-1)/8), ((level->player.plPosY+level->player.height-48)/8));

   
   if ((collisionInd <= 68  && collisionInd >= 65) || (collisionInd <= 167  && collisionInd >= 158))
   {

      return FALSE;
   }
   

   if ((collisionInd > 68  || collisionInd < 65) || (collisionInd > 167  || collisionInd < 158))
   {

      if (level->newCamX > 0)
      {
         level->newCamX -= 1;
         MAP_scrollTo(level->backGroundMap,  level->newCamX , level->newCamY);

         level->player.plPosX -= 1;

      }
      else if (level->player.plPosX >= 16)
      {
         level->player.plPosX -= 1;
         level->player.x -= 1;

      }
     
      SPR_setHFlip(level->player.spriteDef, TRUE);
     if (!level->player.jumpCollision)
     {
      SPR_setAnim(level->player.spriteDef, ANIM_MOVE);

      //setSpriteAnimation(level, ANIM_MOVE, FALSE, 0);
     // level->player.animInd = -1;

     }
     else
     {
      SPR_setAnim(level->player.spriteDef, ANIM_JUMP);
      
     }

   return TRUE;


   }

   return FALSE;
}

bool collisionRightSide(Level *level, u16 value)
{

   if (!(value & BUTTON_RIGHT))
      return FALSE;

   u16 collisionInd = MAP_getTile(level->backGroundMap, ((level->player.plPosX+level->player.width+1)/8), ((level->player.plPosY+level->player.height-8)/8));
   
   if ((collisionInd <= 68  && collisionInd >= 65) || (collisionInd <= 167  && collisionInd >= 158))
      return FALSE;
   collisionInd = MAP_getTile(level->backGroundMap, ((level->player.plPosX+level->player.width+1)/8), ((level->player.plPosY+level->player.height-16)/8));
  
   if ((collisionInd <= 68  && collisionInd >= 65) || (collisionInd <= 167  && collisionInd >= 158))
      return FALSE;
   collisionInd = MAP_getTile(level->backGroundMap, ((level->player.plPosX+level->player.width+1)/8), ((level->player.plPosY+level->player.height-32)/8));
   
   if ((collisionInd <= 68  && collisionInd >= 65) || (collisionInd <= 167  && collisionInd >= 158))
      return FALSE;
   
   collisionInd = MAP_getTile(level->backGroundMap, ((level->player.plPosX+level->player.width+1)/8), ((level->player.plPosY+level->player.height-48)/8));

   
   if ((collisionInd <= 68  && collisionInd >= 65) || (collisionInd <= 167  && collisionInd >= 158))
   {

      return FALSE;
   }
   


   if ((collisionInd > 68  || collisionInd < 65) || (collisionInd > 167  || collisionInd < 158))
   {


      if (level->player.plPosX >= 160)
      {     
         level->newCamX+=1;
         MAP_scrollTo(level->backGroundMap,  level->newCamX , level->newCamY);
         level->player.plPosX +=1;

      }    
      else
      {
         level->player.plPosX +=1;
         level->player.x +=1;

      }
     // level->player.x += 1;

     SPR_setHFlip(level->player.spriteDef, FALSE);
    // SPR_setHFlip(level->player.spriteAction, FALSE);
     if (!level->player.jumpCollision)
     {
      SPR_setAnim(level->player.spriteDef, ANIM_MOVE);

     // setSpriteAnimation(level, ANIM_MOVE, FALSE, 0);
     // level->player.animInd = -1;

     }
     else
     {
      SPR_setAnim(level->player.spriteDef, ANIM_JUMP);

     }

   return TRUE;


   }
   return FALSE;
}
